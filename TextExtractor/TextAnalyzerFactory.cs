﻿using System.IO;
using TextExtractor.Blocks;

namespace TextExtractor
{
    static class TextAnalyzerFactory
    {
        public static IBlock CreateFirstBlock(TextReader reader) => new OtherBlock(reader);
    }
}
