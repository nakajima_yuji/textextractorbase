﻿using System;
using System.IO;

namespace TextExtractor
{
    class Program
    {
        static void Main(string[] args)
        {
            var filename = args[0];
            using(var reader = new StreamReader(filename))
            {
                var dataBug = new DataBug();
                var currentBlock = TextAnalyzerFactory.CreateFirstBlock(reader);
                while ((currentBlock = currentBlock.GetNext()) != null) 
                {
                    currentBlock.ExtractData(dataBug);
                }
            }
        }
    }
}
