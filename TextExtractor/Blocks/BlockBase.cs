﻿using System;
using System.IO;

namespace TextExtractor.Blocks
{
    abstract class BlockBase : IBlock
    {
        protected TextReader Reader { get; }

        public BlockBase(TextReader reader)
        {
            this.Reader = reader;
        }

        public IBlock GetNext()
        {
            var nextLine = Reader.ReadLine();
            return nextLine switch
            {
                null => null,
                _ => GetNext(nextLine),
            };
        }

        public virtual DataBug ExtractData(DataBug src) => src;

        protected abstract IBlock GetNext(string src);
    }
}
