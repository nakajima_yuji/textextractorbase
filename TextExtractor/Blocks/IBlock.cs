﻿namespace TextExtractor.Blocks
{
    interface IBlock
    {
        IBlock GetNext();
        DataBug ExtractData(DataBug src);
    }
}
