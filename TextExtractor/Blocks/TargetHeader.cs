﻿using System.IO;

namespace TextExtractor.Blocks
{
    class TargetHeader : BlockBase
    {
        public TargetHeader(TextReader reader) : base(reader)
        {

        }

        protected override IBlock GetNext(string src)
        {
            if (src.StartsWith("---")) return new TargetDetail(Reader, src);
            return new TargetHeader(Reader);
        }
    }
}
