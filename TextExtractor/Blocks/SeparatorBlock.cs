﻿using System.IO;

namespace TextExtractor.Blocks
{
    class SeparatorBlock : BlockBase
    {
        public SeparatorBlock(TextReader reader) : base(reader)
        {

        }

        protected override IBlock GetNext(string src)
        {
            return src switch
            {
                "Header" => new TargetHeader(Reader),
                _ => new OtherBlock(Reader),
            };
        }
    }
}
