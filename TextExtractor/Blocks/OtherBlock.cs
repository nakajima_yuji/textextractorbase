﻿using System.IO;

namespace TextExtractor.Blocks
{
    class OtherBlock : BlockBase
    {
        public OtherBlock(TextReader reader) : base(reader)
        {

        }

        protected override IBlock GetNext(string src)
        {
            return src switch
            {
                "" => new SeparatorBlock(Reader),
                "Header" => new TargetHeader(Reader),
                _ => new OtherBlock(Reader),
            };
        }
    }
}
