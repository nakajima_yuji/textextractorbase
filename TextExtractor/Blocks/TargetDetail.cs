﻿using System;
using System.IO;

namespace TextExtractor.Blocks
{
    class TargetDetail : BlockBase
    {
        private readonly string currentLine;

        public TargetDetail(TextReader reader, string src) : base(reader)
        {
            currentLine = src;
        }

        public override DataBug ExtractData(DataBug src)
        {
            var normalizedSrc = currentLine.Trim();
            var splitSrc = normalizedSrc.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            src.Rate = Convert.ToDouble(splitSrc[1]);
            return src;
        }

        protected override IBlock GetNext(string src)
        {
            return src switch
            {
                "" => new SeparatorBlock(Reader),
                _ => new TargetDetail(Reader, src),
            };
        }
    }
}
