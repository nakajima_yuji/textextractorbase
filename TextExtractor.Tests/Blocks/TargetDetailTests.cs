﻿using System.IO;
using System.Text;
using TextExtractor.Blocks;
using Xunit;

namespace TextExtractor.Tests.Blocks
{
    public class TargetDetailTests
    {
        [Fact]
        public void GetNext_SetSeparatorString()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine(string.Empty);
            using var reader = new StringReader(sb.ToString());
            var obj = new TargetDetail(reader, null);

            // Act
            var actual = obj.GetNext();

            // Assert
            Assert.IsType<SeparatorBlock>(actual);
        }

        [Fact]
        public void GetNext_SetOtherString()
        {
            // Arrange
            using var reader = new StringReader("Other");
            var obj = new TargetDetail(reader, null);

            // Act
            var actual = obj.GetNext();

            // Assert
            Assert.IsType<TargetDetail>(actual);
        }

        [Fact]
        public void ExtractData_SetRate()
        {
            // Arrange
            var obj = new TargetDetail(null, "  keyword    1.0  ");
            var db = new DataBug();
            
            // Act
            obj.ExtractData(db);

            // Assert
            Assert.Equal(1.0d, db.Rate);
        }
    }
}
