﻿using System.IO;
using TextExtractor.Blocks;
using Xunit;

namespace TextExtractor.Tests.Blocks
{
    public class TargetHeaderTests
    {
        [Fact]
        public void GetNext_SetChangeToDetailString()
        {
            // Arrange
            using var reader = new StringReader("--------");
            var obj = new TargetHeader(reader);

            // Act
            var actual = obj.GetNext();

            // Assert
            Assert.IsType<TargetDetail>(actual);
        }

        [Fact]
        public void GetNext_SetOtherString()
        {
            // Arrange
            using var reader = new StringReader("other");
            var obj = new TargetHeader(reader);

            // Act
            var actual = obj.GetNext();

            // Assert
            Assert.IsType<TargetHeader>(actual);
        }
    }
}
