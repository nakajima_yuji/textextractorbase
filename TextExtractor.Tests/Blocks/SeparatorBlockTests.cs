﻿using System.IO;
using TextExtractor.Blocks;
using Xunit;

namespace TextExtractor.Tests.Blocks
{
    public class SeparatorBlockTests
    {
        [Fact]
        public void GetNext_SetChangeToHeaderString()
        {
            // Arrange
            using var reader = new StringReader("Header");
            var obj = new SeparatorBlock(reader);

            // Act
            var actual = obj.GetNext();

            // Assert
            Assert.IsType<TargetHeader>(actual);
        }

        [Fact]
        public void GetNext_SetOtherText()
        {
            // Arrange
            using var reader = new StringReader("Other");
            var obj = new SeparatorBlock(reader);

            // Act
            var actual = obj.GetNext();

            // Assert
            Assert.IsType<OtherBlock>(actual);
        }
    }
}
