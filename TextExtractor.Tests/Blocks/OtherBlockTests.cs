using System.IO;
using System.Text;
using TextExtractor.Blocks;
using Xunit;

namespace TextExtractor.Tests.Blocks
{
    public class OtherBlockTests
    {
        [Fact]
        public void GetNext_SetSeparatorString()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine(string.Empty);
            using var reader = new StringReader(sb.ToString());
            var obj = new OtherBlock(reader);

            // Act
            var actual = obj.GetNext();

            // Assert
            Assert.IsType<SeparatorBlock>(actual);
        }

        [Fact]
        public void GetNext_SetChangeToHeaderString()
        {
            // Arrange
            using var reader = new StringReader("Header");
            var obj = new OtherBlock(reader);

            // Act
            var actual = obj.GetNext();

            // Assert
            Assert.IsType<TargetHeader>(actual);
        }

        [Fact]
        public void GetNext_SetOtherString()
        {
            // Arrange
            using var reader = new StringReader("Other");
            var obj = new OtherBlock(reader);

            // Act
            var actual = obj.GetNext();

            // Assert
            Assert.IsType<OtherBlock>(actual);
        }
    }
}
